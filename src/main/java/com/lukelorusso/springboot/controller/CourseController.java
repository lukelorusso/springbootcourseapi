package com.lukelorusso.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lukelorusso.springboot.model.Course;
import com.lukelorusso.springboot.model.Topic;
import com.lukelorusso.springboot.service.CourseService;
import com.lukelorusso.springboot.service.TopicService;

@RestController
public class CourseController {
  
  @Autowired
  private CourseService courseService;
  
  @Autowired
  private TopicService topicService;

  @GetMapping("/courses")
  public List<Course> getAllCourses() {
    //return courseService.getCourses();
    
    // This solution will help not having a recursive-infinite JSON as response
    List<Course> courseList = courseService.getCourses();
    for (Course c : courseList) {
      c.getTopic().setCourseList(null);
    }
    return courseList;
  }

  @GetMapping("/courses/{id}")
  public Course getCourse(@PathVariable String id) {
    return courseService.getCourse(id);
  }
  
  @PostMapping("/courses")
  public void addCourse(@RequestBody Course course) {
    if (course.getTopic() != null) {
      String topicId = course.getTopic().getId();
      Topic topic = topicService.getTopic(topicId);
      course.setTopic(topic);
    }
    courseService.addCourse(course);
  }
  
  @PutMapping("/courses/{id}")
  public Course updateCourse(@RequestBody Course course, @PathVariable String id) {
    course = courseService.updateCourse(id, course);
    /*if (course == null) {
      
    }*/
    return course;
  }
  
  @DeleteMapping("/courses/{id}")
  public void deleteCourse(@PathVariable String id) {
	  courseService.deleteCourse(id);
  }

}
