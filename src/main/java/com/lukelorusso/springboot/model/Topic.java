package com.lukelorusso.springboot.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "topic")
public class Topic {
  
  @Id
  private String id;
  
  private String name;
  
  private String description;
  
  @OneToMany(mappedBy = "topic", cascade = {
      CascadeType.PERSIST,
      CascadeType.MERGE,
      CascadeType.DETACH,
      CascadeType.REFRESH
  })
  private List<Course> courseList;
  
  public Topic() {}

  public Topic(String id, String name, String description) {
    super();
    this.id = id;
    this.name = name;
    this.description = description;
    this.courseList = new ArrayList<>();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<Course> getCourseList() {
    return courseList;
  }

  public void setCourseList(List<Course> courseList) {
    this.courseList = courseList;
  }
  
  /**
   * Use this to add a course
   */
  public void add(Course course) {
    if (course != null) {
      if (this.courseList == null) {
        this.courseList = new ArrayList<>();
      }
      this.courseList.add(course);
      course.setTopic(this);
    }
  }

}
