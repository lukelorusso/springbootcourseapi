package com.lukelorusso.springboot.repository;

import org.springframework.data.repository.CrudRepository;

import com.lukelorusso.springboot.model.Topic;

public interface TopicRepository extends CrudRepository<Topic, String> {

}
