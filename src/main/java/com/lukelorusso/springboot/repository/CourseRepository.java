package com.lukelorusso.springboot.repository;

import org.springframework.data.repository.CrudRepository;

import com.lukelorusso.springboot.model.Course;

public interface CourseRepository extends CrudRepository<Course, String> {

}
