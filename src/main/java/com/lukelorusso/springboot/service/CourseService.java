package com.lukelorusso.springboot.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lukelorusso.springboot.model.Course;
import com.lukelorusso.springboot.repository.CourseRepository;

@Service
public class CourseService {
  
  @Autowired
  private CourseRepository courseRepository;

  public List<Course> getCourses() {
    List<Course> courseList = new ArrayList<>();
    courseRepository.findAll().forEach(courseList::add);
    return courseList;
  }

  public Course getCourse(String id) {
    return courseRepository.findById(id).get();
  }
  
  public void addCourse(Course course) {
    saveCourse(course);
  }

  public Course updateCourse(String id, Course course) {
    return saveCourse(course);
  }

  public void deleteCourse(String id) {
    Course course = getCourse(id);
    courseRepository.delete(course);
  }
  
  public Course saveCourse(Course course) {
    courseRepository.save(course);
    return course;
  }
  
  

}
