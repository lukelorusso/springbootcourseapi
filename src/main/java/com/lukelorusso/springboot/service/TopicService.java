package com.lukelorusso.springboot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lukelorusso.springboot.model.Topic;
import com.lukelorusso.springboot.repository.TopicRepository;

@Service
public class TopicService {
  
  @Autowired
  private TopicRepository topicRepository;
  
  /*private List<TopicModel> topics = new ArrayList<>(Arrays.asList(
      new TopicModel("spring", "Spring Framework", "Spring Framework Description"),
      new TopicModel("java", "Core Java", "Core Java Description"),
      new TopicModel("javascript", "JavaScript", "JavaScript Description")
      ));*/

  public List<Topic> getTopics() {
    //return topics;
    List<Topic> topicList = new ArrayList<>();
    topicRepository.findAll().forEach(topicList::add);
    return topicList;
  }

  public Topic getTopic(String id) {
    /*return topics
        .stream()
        .filter(t -> t.getId().equals(id))
        .findFirst()
        .get();*/
    return topicRepository.findById(id).get();
  }
  
  public void addTopic(Topic topic) {
    //topics.add(topic);
    saveTopic(topic);
  }

  public Topic updateTopic(String id, Topic topic) {
    /*for (int i = 0; i < topics.size(); i++) {
      TopicModel temp = topics.get(i);
      if (temp.getId().equals(id)) {
        topics.set(i, topic);
        return topics.get(i);
      }
    }
    return null;*/
    return saveTopic(topic);
  }

  public void deleteTopic(String id) {
    //topics.removeIf(t -> t.getId().equals(id));
    Topic topic = getTopic(id);
    topicRepository.delete(topic);
  }
  
  public Topic saveTopic(Topic topic) {
    topicRepository.save(topic);
    return topic;
  }
  
  

}
